from HTMLParser import HTMLParser

class Stripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        data = d.rstrip()
        if not data == '':
            self.fed.append(data)
    def get_data(self, glue = ','):
        return glue.join(self.fed)
    def get_data_as_array(self):
        return self.fed

def strip_tags(html, glue = ','):
    s = Stripper()
    s.feed(html)
    return s.get_data(glue)