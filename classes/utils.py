# Standard libs.
import ConfigParser
import os.path
import urllib

# Standard libs for logging stuff. Thanks RocketMap.
import logging
import sys
import os

formatter = logging.Formatter(
    '%(asctime)s [%(threadName)18s][%(module)14s][%(levelname)8s] %(message)s')

# Redirect messages lower than WARNING to stdout
stdout_hdlr = logging.StreamHandler(sys.stdout)
stdout_hdlr.setFormatter(formatter)
stdout_hdlr.setLevel(5)

# Redirect messages equal or higher than WARNING to stderr
stderr_hdlr = logging.StreamHandler(sys.stderr)
stderr_hdlr.setFormatter(formatter)
stderr_hdlr.setLevel(logging.WARNING)

log = logging.getLogger()
log.setLevel(logging.DEBUG)
log.addHandler(stdout_hdlr)
log.addHandler(stderr_hdlr)

# Try load MySQL.
canmysql = True
try:
    import MySQLdb
    log.info('MySQLdb is available.')
except ImportError, e:
    log.info('MySQLdb is not available. Just scraping data.')
    canmysql = False

class Config:
    def __init__(self):
        rootpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

        if not os.path.exists(rootpath + '/config.ini'):
            raise Exception('One must simply have a config.ini file, ok');
        
        self.config = ConfigParser.ConfigParser()
        self.config.read(rootpath + '/config.ini')
        
        # Check the config for a default section.
        if not self.config.has_section('default'):
            raise Exception('Ones config must simply have a default section')

        # Check the config default section for an url.
        if not self.config.has_option('default', 'url'):
            raise Exception('One must simply have an url specified in the default section of the config.ini file.')

        # Ensure that the url is reachable.
        responsecode = urllib.urlopen(self.get_config('default', 'url')).getcode()
        if not responsecode == 200:
            message = 'URL Error (' + format(responsecode) + '): ' + self.get_config('default', 'url')
            raise Exception(message)

        if canmysql:
            # Check the db.
            if not self.config.has_section('database'):
                raise Exception('Ones config must have a database section.')

            if not self.config.has_option('database', 'dbname'):
                raise Exception('One must simply have a dbname specified in the database section of the config.ini file.')

            if not self.config.has_option('database', 'dbuser'):
                raise Exception('One must simply have a dbuser specified in the database section of the config.ini file.')

            if not self.config.has_option('database', 'dbpass'):
                raise Exception('One must simply have a dbpass specified in the database section of the config.ini file.')

            if not self.config.has_option('database', 'dbhost'):
                raise Exception('One must simply have a dbhost specified in the database section of the config.ini file.')

            # Ensure one can connect to the db.
            db = MySQLdb.connect(
                self.get_config('database', 'dbhost'),
                self.get_config('database', 'dbuser'),
                self.get_config('database', 'dbpass'),
                self.get_config('database', 'dbname')
            )

            # Setup the tables.
            dbcursor = db.cursor()

            table = dbcursor.execute("SHOW TABLES LIKE 'wp_nzmmna_board_members'")

            if table == 0:
                sql = """CREATE TABLE wp_nzmmna_board_members (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    firstname VARCHAR(40) NOT NULL,
                    lastname VARCHAR(40) NOT NULL,
                    email VARCHAR(80) NOT NULL,
                    position VARCHAR(20) NOT NULL,
                    imageurl VARCHAR(2083),
                    timemodified INT(10)
                )"""

                try:
                    log.info("Creating table %s in %s.", 'wp_nzmmna_board_members', self.get_config('database', 'dbname'))
                    dbcursor.execute(sql)
                    db.commit()
                except Exception, e:
                    db.rollback()
                    raise Exception(e)

            # Check for column imageurl and add if necessary.
            columnsql = """SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '<dbname>' AND TABLE_NAME = 'wp_nzmmna_board_members' AND COLUMN_NAME = 'imageurl'"""
            columnsql = columnsql.replace('<dbname>', self.get_config('database', 'dbname'))

            column = dbcursor.execute(columnsql)

            if column == 0:
                sqlupdatetable = """ALTER TABLE wp_nzmmna_board_members ADD COLUMN imageurl VARCHAR(2083)"""
                
                try:
                    log.info("Adding column %s.", 'imageurl')
                    dbcursor.execute(sqlupdatetable)
                    db.commit()
                except Exception, e:
                    db.rollback()
                    raise Exception(e)

            # Check for column timemodified and add if necessary.
            columnsql = """SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '<dbname>' AND TABLE_NAME = 'wp_nzmmna_board_members' AND COLUMN_NAME = 'timemodified'"""
            columnsql = columnsql.replace('<dbname>', self.get_config('database', 'dbname'))

            column = dbcursor.execute(columnsql)

            if column == 0:
                sqlupdatetable = """ALTER TABLE wp_nzmmna_board_members ADD COLUMN timemodified int(10)"""

                try:
                    log.info("Adding column %s.", 'timemodified')
                    dbcursor.execute(sqlupdatetable)
                    db.commit()
                except Exception, e:
                    db.rollback()
                    raise Exception(e)

            # Close the connection.
            db.close()
    def get_config(self, section, name):
        config = self.config.get(section, name)
        
        if not config:
            raise Exception('The config ' + name + ' in section ' + section + ' does not exist.')

        return config
