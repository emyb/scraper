# Not local stuff.
import lxml.html
from lxml.cssselect import CSSSelector
import requests

# Local stuff.
from Stripper import Stripper
from Stripper import strip_tags

class Pager(object):
    def __init__(self, url):
        self.url = url
        self.set_page()
        self.set_dom()
    def set_page(self):
        self.page = requests.get(self.url)
    def set_dom(self):
        self.dom = lxml.html.fromstring(self.page.text)
    def get_dom(self):
        if not hasattr(self, 'dom'):
            self.set_dom()
        return self.dom
    def set_cssselector(self, selector):
        self.cssselector = CSSSelector(selector)
    def get_cssselector(self):
        if not hasattr(self, 'cssselector'):
            return "Stop being stupid"
        return self.cssselector
    def get_elements(self, selector):
        return selector(self.get_dom())
    def get_contents(self, elements, trim = True, strip = True):
        if strip:
            contents = strip_tags(lxml.html.tostring(elements))
        else:
            contents = lxml.html.tostring(elements)

        if trim:
            return contents.strip()
        else:
            return contents
    def get_contents_as_array(self, elements):
        stripper = Stripper()
        stripper.feed(lxml.html.tostring(elements))
        return stripper.get_data_as_array()