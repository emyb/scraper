# This will grab the names, positions and email addresses 
# for the board members of NZMMNA.

# Grab some libs.
import time

# Standard libs for logging stuff. Thanks RocketMap.
import logging
import sys

formatter = logging.Formatter(
    '%(asctime)s [%(threadName)18s][%(module)14s][%(levelname)8s] %(message)s')

# Redirect messages lower than WARNING to stdout
stdout_hdlr = logging.StreamHandler(sys.stdout)
stdout_hdlr.setFormatter(formatter)
stdout_hdlr.setLevel(5)

# Redirect messages equal or higher than WARNING to stderr
stderr_hdlr = logging.StreamHandler(sys.stderr)
stderr_hdlr.setFormatter(formatter)
stderr_hdlr.setLevel(logging.WARNING)

log = logging.getLogger()
log.setLevel(logging.DEBUG)
log.addHandler(stdout_hdlr)
log.addHandler(stderr_hdlr)

# Try to load libs.
try:
    from classes.Pager import Pager
    from classes.utils import Config
except ImportError, e:
    log.critical("Some stuff isn't installed. Please run " +
        "pip install --upgrade -r requirements.txt.")
    sys.exit(1)

# Try load MySQL.
canmysql = True
try:
    import MySQLdb
    log.info('MySQLdb is avaliable.')
except ImportError, e:
    log.info('MySQLdb is not available. Just scraping data.')
    canmysql = False

# Validate the config. This also validates the url.
config = Config()

page = Pager(config.get_config('default', 'url')+'/contact.html')

# Make a MySQL thing.
if canmysql:
    db = MySQLdb.connect(
        config.get_config('database', 'dbhost'),
        config.get_config('database', 'dbuser'),
        config.get_config('database', 'dbpass'),
        config.get_config('database', 'dbname')
    )

cssselectors = [
    "div.wsite-section-content div.container div:nth-child(4) table tbody tr td:nth-child(1)",
    "div.wsite-section-content div.container div:nth-child(4) table tbody tr td:nth-child(2)",
    "div.wsite-section-content div.container div:nth-child(4) table tbody tr td:nth-child(3)"
]

details = []

for cssselector in cssselectors:
    page.set_cssselector(cssselector)
    selector    = page.get_cssselector()
    elements    = page.get_elements(selector)[0][0]
    contents    = page.get_contents(elements)

    details.append(page.get_contents_as_array(elements))

# The names of the positions.
positions = details[0]

# The names of the people who hold the position.
positionholders = details[1]

# The email addresses of the people who hold the position.
positionholdersemails = details[2]

# The number of position holders should be aligned with the number of their emails addresses or else.
numberofpositions               = len(positions)
numberofpositionholders         = len(positionholders)
numberofpositionholdersemails   = len(positionholdersemails)

# These just better be bloody aligned.
if not numberofpositionholdersemails == numberofpositionholders:
    print positionholders
    print positionholdersemails

    message = "Found "+format(numberofpositionholders)+" position holders and only "+format(numberofpositionholdersemails)+" email addresses. This shouldn't have happend."
    raise Exception(message)

if canmysql:
    dbcursor = db.cursor()

for i in range(numberofpositionholders):
    if i == 0:
        log.info("Skipping first row that contains column headers i.e. %s %s %s.", positions[i], positionholders[i], positionholdersemails[i])
        # Skip the first elements.
        if False:
            # One day this would be a very verbose message, or added to an array of stuff and displayed later.
            print "{:<20} {:<20} {:<30}".format(positions[i], positionholders[i], positionholdersemails[i])
        continue

    # Prepare some vars.
    names       = positionholders[i].split()
    firstname   = names[0]
    lastname    = names[1]
    email       = positionholdersemails[i]

    if canmysql:
        firstname   = db.escape_string(firstname)
        lastname    = db.escape_string(lastname)
        email       = db.escape_string(email)

    # The main positions.
    if i < numberofpositions:
        # Set the position.
        position    = positions[i]

        if canmysql:
            position = db.escape_string(position)

        # Change from plural Committee Members to just Committee Member.
        if position.lower() == 'committee members':
            if canmysql:
                log.info("Position %s is being changed to a not plural word.", position)
            
            position = 'Committee Member'

        if not canmysql:
            # One day this would be a very verbose message, or added to an array of stuff and displayed later.
            log.info("{:<20} {:<20} {:<30}".format(position, positionholders[i], positionholdersemails[i]))
    else:    
        # The non main positions.
        position = positions[numberofpositions - 1]

        if canmysql:
            position = db.escape_string(position)

        # Change from plural Committee Members to just Committee Member.
        if position.lower() == 'committee members':
            if canmysql:
                log.info("Position %s is being changed to a not plural word.", position)
            position = 'Committee Member'
        
        if not canmysql:
            # One day this would be a very verbose message, or added to an array of stuff and displayed later.
            log.info("{:<20} {:<20} {:<30}".format(position, positionholders[i], positionholdersemails[i]))

    if position.lower() == 'committee member':
        if canmysql:
            log.info("Checking for an existing record for %s %s %s.", position, firstname, lastname)
        
            # Try by name.
            sqlexistingname = """SELECT * FROM wp_nzmmna_board_members 
            WHERE firstname = '<firstname>' AND lastname = '<lastname>'"""

            sqlexistingname = sqlexistingname.replace('<firstname>', firstname)
            sqlexistingname = sqlexistingname.replace('<lastname>', lastname)

            # Check for existing email.
            sqlexistingemail = """SELECT * FROM wp_nzmmna_board_members
            WHERE email = '<email>'"""

            sqlexistingemail = sqlexistingemail.replace('<email>', email)

            result = 0

            if dbcursor.execute(sqlexistingname):
                log.info("Found a record for %s %s %s.", position, firstname, lastname)
                result = dbcursor.execute(sqlexistingname)
            elif dbcursor.execute(sqlexistingemail):
                log.info("Could not find a record for %s %s %s.", position, firstname, lastname)
                result = dbcursor.execute(sqlexistingemail)
    else:
        if canmysql:
            log.info("Checking for an existing record for %s %s %s.", position, firstname, lastname)
            # Check if exists by position.
            sqlexisting = """SELECT * FROM wp_nzmmna_board_members
            WHERE position = '<position>'"""

            # Finish preparation of the SQL statement to find stuff.
            sqlexisting = sqlexisting.replace('<position>', position)
            result = dbcursor.execute(sqlexisting)

            record = dbcursor.fetchone()

            # SQL for the new.
            if not record:
                log.info("Inserting record for %s %s, email %s, position %s.", firstname, lastname, email, position)
                sql = """INSERT INTO wp_nzmmna_board_members (firstname, lastname, email, position, timemodified) VALUES
                    ('<firstname>', '<lastname>', '<email>', '<position>', <timemodified>)"""
            else:
                log.info("Updating record for %s %s, email %s, position %s.", firstname, lastname, email, position)
                sql = """UPDATE wp_nzmmna_board_members 
                SET firstname = '<firstname>', 
                lastname = '<lastname>', 
                email = '<email>', 
                position = '<position>',
                timemodified = '<timemodified>'
                WHERE id = """ + format(record[0])

            timemodified = int(time.time())

            sql = sql.replace('<firstname>', firstname)
            sql = sql.replace('<lastname>', lastname)
            sql = sql.replace('<email>', email)
            sql = sql.replace('<position>', position)
            sql = sql.replace('<timemodified>', str(timemodified))
        
            try:
                # Insert or update.
                dbcursor.execute(sql)
                db.commit()
            except Exception, e:
                log.warning("Failed to execute query\n\t%s.", sql)
                # Rollback incase of issues.
                db.rollback()

message = "Scraping finished."

if canmysql:
    message = message.replace('.', '') + " and db connection closed."
    db.close()

log.info(message)