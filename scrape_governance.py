# This will grab the names, positions and email addresses 
# for the board members of NZMMNA.

# Standard libs for logging stuff. Thanks RocketMap.
import logging
import sys
import time

formatter = logging.Formatter(
    '%(asctime)s [%(threadName)18s][%(module)14s][%(levelname)8s] %(message)s')

stdout_hdlr = logging.StreamHandler(sys.stdout)
stdout_hdlr.setFormatter(formatter)

stderr_hdlr = logging.StreamHandler(sys.stderr)
stderr_hdlr.setFormatter(formatter)
stderr_hdlr.setLevel(logging.WARNING)

log = logging.getLogger()
log.addHandler(stdout_hdlr)
log.addHandler(stderr_hdlr)

# MySQL stuff.
canmysql = True
try:
    import MySQLdb
    log.info('MySQLdb is available.')
except ImportError, e:
    log.info('MySQLdb is not available. Just scraping data.')
    canmysql = False

# Try to load libs.
try:
    from classes.Pager import Pager
    from classes.utils import Config
except ImportError, e:
    log.critical("Some stuff isn't installed. Please run " +
        "pip install --upgrade -r requirements.txt.")
    sys.exit(1)

# Validate the config. This also validates the url.
config = Config()

page = Pager(config.get_config('default', 'url')+'/governance.html')

# Make a MySQL thing.
if canmysql:
    db = MySQLdb.connect(
        config.get_config('database', 'dbhost'),
        config.get_config('database', 'dbuser'),
        config.get_config('database', 'dbpass'),
        config.get_config('database', 'dbname')
    )

    # Setup the tables.
    dbcursor = db.cursor()

    table = dbcursor.execute("SHOW TABLES LIKE 'wp_nzmmna_governance_documents'")

    if table == 0:
        sql = """CREATE TABLE wp_nzmmna_governance_documents (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            documentname VARCHAR(128) NOT NULL,
            documenturl VARCHAR(2083) NOT NULL,
            timemodified INT(10)
        )"""

        try:
            log.info("Creating table %s in %s.", 'wp_nzmmna_governance_documents', config.get_config('database', 'dbname'))
            dbcursor.execute(sql)
            db.commit()
        except Exception, e:
            db.rollback()
            raise Exception(e)

page.set_cssselector("a.wsite-button.wsite-button-small.wsite-button-highlight")
selector    = page.get_cssselector()
elements    = page.get_elements(selector)

log.info("{:<50} {:<70} {:<30}".format('Base URL', 'href', 'Content'))

for element in elements:
    # Get the document name.
    documentname = page.get_contents(element)

    # Get the document url.
    documenturl = config.get_config('default', 'url') + element.attrib['href']

    # Transform for insertion to mysql.
    if canmysql:
        documentname    = db.escape_string(documentname)
        documenturl     = db.escape_string(documenturl)

    log.info("{:<50} {:<70} {:<30}".format(config.get_config('default', 'url'), element.attrib['href'], documentname))

    if canmysql:
        # Check for an existing record by the document name.
        existingsql = """SELECT * FROM wp_nzmmna_governance_documents
        WHERE documentname = '<documentname>'"""
        existingsql = existingsql.replace('<documentname>', documentname)

        log.info("Checking for an existing record for %s.", documentname)

        result = dbcursor.execute(existingsql)
        record = dbcursor.fetchone()

        # Check for a record by document url.
        if not record:
            # Check for an existing record by the document url.
            existingsql = """SELECT * FROM wp_nzmmna_governance_documents
            WHERE documenturl = '<documenturl>'"""
            existingsql = existingsql.replace('<documenturl>', documenturl)

            log.info("Checking for an existing record for %s.", documenturl)

        # Need to add a record.
        if not record:
            log.info("Inserting record for %s, %s", documentname, documenturl)
            sql = """INSERT INTO wp_nzmmna_governance_documents (documentname, documenturl, timemodified) VALUES
                ('<documentname>', '<documenturl>', '<timemodified>')"""
        else:
            log.info("Updating record for %s, %s", documentname, documenturl)
            sql = """UPDATE wp_nzmmna_governance_documents
            SET documentname = '<documentname>',
            documenturl = '<documenturl>',
            timemodified = '<timemodified>'
            WHERE id = """ + format(record[0])

        timemodified = int(time.time())

        sql = sql.replace('<documentname>', documentname)
        sql = sql.replace('<documenturl>', documenturl)
        sql = sql.replace('<timemodified>', str(timemodified))

        try:
            # Insert or update.
            dbcursor.execute(sql)
            log.info(sql)
            db.commit()
        except Exception, e:
            # Rollback incase of issues.
            log.warning("Failed to execute query\n\t%s", sql)
            db.rollback()

message = "Scraping finished."

if canmysql:
    db.close()
    message = message.replace('.', '') + " and db connection closed."

log.info(message)